//
//  AppCoordinator.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 21/11/20.
//

import UIKit

class AppCoordinator {

    private let window: UIWindow

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        guard let siteViewController = SitesViewController.instantiate() else {
            return
        }
        let navigationController = UINavigationController(rootViewController: siteViewController)

        if KeychainManager.shared.siteID != nil,
           let homeViewController = HomeViewController.instantiate() {
            navigationController.viewControllers.append(homeViewController)
        }
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}
