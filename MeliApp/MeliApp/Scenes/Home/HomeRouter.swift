//
//  HomeRouter.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

protocol HomeRoutingLogic {
    func navigateToProductsByCategory(categoryModel: CategoryModel)
    func navigateToProductDetail(product: ProductModel)
}

// MARK: - HomeRoutingLogic

final class HomeRouter: NSObject, HomeRoutingLogic {

    weak var viewController: HomeViewController?

    func  navigateToProductDetail(product: ProductModel) {
        guard let navigationController = viewController?.navigationController,
              let controller = ProductDetailViewController.instantiate() as? ProductDetailViewController else {
            return
        }
        controller.interactor?.product = product
        navigationController.pushViewController(controller, animated: true)
    }

    func navigateToProductsByCategory(categoryModel: CategoryModel) {
        guard let navigationController = viewController?.navigationController,
              let controller = ProductsViewController.instantiate() as? ProductsViewController else {
            return
        }
        controller.interactor?.category = categoryModel
        navigationController.pushViewController(controller, animated: true)
    }
}
