//
//  HomeInteractor.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 21/11/20.
//

import UIKit

protocol HomeBusinessLogic {
    func prepareCategories()
    func filterContent(forQuery query: String?)
}

protocol HomeDataStore {
    var isSearching: Bool { get set }
    var categories: [CategoryModel] { get }
}

final class HomeInteractor: HomeDataStore {

    var presenter: HomePresentationLogic?

    private let service: CategoryService

    // MARK: - HomeDataStore

    var isSearching: Bool = false
    var categories: [CategoryModel] = []

    init(service: CategoryService) {
        self.service = service
    }
}

// MARK: - HomeBusinessLogic

extension HomeInteractor: HomeBusinessLogic {

    func prepareCategories() {
        isSearching = false
        service.fetchCategories { [weak self] (categoriesModel) in
            guard let categoriesModel = categoriesModel else {
                return
            }
            self?.categories = categoriesModel
            guard let response = self?.createSectionsResponse(productsResponse: []) else {
                return
            }
            self?.presenter?.presentSections(response)
        }
    }

    func filterContent(forQuery query: String?) {
        guard let query = query, !query.isEmpty else {
            let response = createSectionsResponse(productsResponse: [])
            presenter?.presentSections(response)
            return
        }
        isSearching = true
        service.fetchProducts(byQuery: query) { [weak self] (productResponse) in
            guard let productResponse = productResponse else {
                return
            }
            guard let response = self?.createSectionsResponse(productsResponse: productResponse.results ?? []) else {
                return
            }
            self?.presenter?.presentSections(response)
        }
    }
}

// MARK: - Private functions

private extension HomeInteractor {

    func createSectionsResponse(productsResponse: [ProductModel]) -> HomeSectionsResponse {
        var items: [HomeSection] = categories.map { category -> HomeSection in
            HomeCategorySection(for: .categories, category: category)
        }

        if !productsResponse.isEmpty {
            items.insert(contentsOf: productsResponse.map({ (product) -> HomeSection in
                HomeSearchSection(for: .search, product: product)
            }), at: 0)
        }
        return HomeSectionsResponse(sections: items)
    }
}
