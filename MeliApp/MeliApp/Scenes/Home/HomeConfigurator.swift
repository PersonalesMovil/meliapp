//
//  HomeConfigurator.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 21/11/20.
//

import Foundation

struct HomeConfigurator {

    static func configure(_ viewController: HomeViewController) {
        let service = CategoryService()
        let interactor = HomeInteractor(service: service)
        let presenter = HomePresenter()
        let router = HomeRouter()
        viewController.interactor = interactor
        interactor.presenter = presenter
        viewController.router = router
        router.viewController = viewController
        presenter.viewController = viewController
    }

}
