//
//  HomePresenter.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 21/11/20.
//

import UIKit

protocol HomePresentationLogic {
    func presentSections(_ response: HomeSectionsResponse)
}

final class HomePresenter {
    weak var viewController: HomeDisplayLogic?
}

// MARK: - HomePresentationLogic

extension HomePresenter: HomePresentationLogic {

    func presentSections(_ response: HomeSectionsResponse) {
        let enumerated = response.sections.enumerated()
        let sectionsViewModels: [HomeSectionViewModel] = enumerated.map { (_, section) in
            var sectionViewModel: HomeSectionViewModel
            sectionViewModel = HomeSectionViewModel(with: section)
            switch section.type {
            case .categories:
                sectionViewModel.rowHeight = UITableView.automaticDimension
            case .search:
                sectionViewModel.rowHeight = UITableView.automaticDimension
            }
            return sectionViewModel
        }
        self.viewController?.displaySections(sectionsViewModels)
    }
}
