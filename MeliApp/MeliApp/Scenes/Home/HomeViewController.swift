//
//  ViewController.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 21/11/20.
//

import UIKit

protocol HomeDisplayLogic: class {
    func displaySections(_ viewmodel: [HomeSectionViewModel])
}

final class HomeViewController: UIViewController {

    var interactor: (HomeBusinessLogic & HomeDataStore)?
    var router: HomeRoutingLogic?
    var isopenSearch: Bool = false

    private var dataProvider = HomeDataProvider(rows: [])

    // MARK: - IBOutlets

    @IBOutlet private weak var tableView: UITableView!

    lazy var searchController: UISearchController = {
        UISearchController()
    }()

    private lazy var refreshControl: UIRefreshControl = { [unowned self] in
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(refreshHome(sender:)), for: .valueChanged)
        return refreshControl
    }()

    static func instantiate() -> UIViewController? {
        let storyboard = UIStoryboard(name: "Home", bundle: .main)
        let viewController = storyboard.instantiateInitialViewController()
        return viewController
    }

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupNavigation()
        interactor?.prepareCategories()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationItem.hidesSearchBarWhenScrolling = true
    }
}

// MARK: - HomeDisplayLogic

extension HomeViewController: HomeDisplayLogic {

    func displaySections(_ viewmodel: [HomeSectionViewModel]) {
        dataProvider.update(rows: viewmodel)
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

// MARK: - Private methods

private extension HomeViewController {

    // MARK: Setup

    func setup() {
        HomeConfigurator.configure(self)
    }

    func setupNavigation() {
        navigationItem.title = "Categorias"
        navigationItem.searchController = searchController
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        navigationItem.hidesSearchBarWhenScrolling = false
    }

    func setupTableView() {
        tableView.registerCells([CategoryTableViewCell.self,
                                 ProductTableViewCell.self])
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .white
        tableView.refreshControl = refreshControl
    }

    // MARK: - Actions

    @objc
    func refreshHome(sender _: UIRefreshControl) {
        interactor?.prepareCategories()
        refreshControl.endRefreshing()
    }
}

// MARK: - UISearchResultsUpdating
extension HomeViewController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        navigationItem.title = "Buscador de productos"
        guard isopenSearch else {
            return
        }
        interactor?.filterContent(forQuery: searchController.searchBar.text)
    }
}

// MARK: - UISearchBarDelegate
extension HomeViewController: UISearchControllerDelegate {

    func willPresentSearchController(_ searchController: UISearchController) {
        isopenSearch = true
    }

    func willDismissSearchController(_ searchController: UISearchController) {
        isopenSearch = false
    }

    func didDismissSearchController(_ searchController: UISearchController) {
        guard let text = searchController.searchBar.text, !text.isEmpty else {
            navigationItem.title = "Categorias"
            return
        }
        navigationItem.title = "resultado de busqueda"
    }
}

// MARK: - UITableViewDataSource
extension HomeViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        dataProvider.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataProvider.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // swiftlint:disable:next force_unwrapping
        let viewModel = self.dataProvider[indexPath]!
        guard let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.reuseIdentifier) else {
            fatalError("Home Section Cell Not Found - Reuse identifier: \(viewModel.reuseIdentifier)")
        }
        guard let configurableCell = cell as? HomeSectionConfigurable else {
            fatalError("Home Section Cell Must Conform with HomeSectionConfigurable")
        }
        configurableCell.configure(viewModel: viewModel)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // swiftlint:disable:next force_unwrapping
        let viewModel = self.dataProvider[indexPath]!
        switch viewModel.type {
        case .categories:
            guard let homeCategorySection = viewModel.model as? HomeCategorySection else {
                return
            }
            router?.navigateToProductsByCategory(categoryModel: homeCategorySection.item)
        case .search:
            guard let homeCategorySection = viewModel.model as? HomeSearchSection else {
                return
            }
            router?.navigateToProductDetail(product: homeCategorySection.item)
        }
    }
}
