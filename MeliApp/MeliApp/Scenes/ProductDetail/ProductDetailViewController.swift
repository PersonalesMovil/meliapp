//
//  ProductDetailViewController.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit

protocol ProductDetailDisplayLogic: class {
    func displayDetails(_ details: ProductRepresentable)
}

final class ProductDetailViewController: UIViewController {

    // MARK: - IBOutlets

    @IBOutlet private weak var productImage: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!

    var interactor: (ProductDetailBusinessLogic & productDataStore)?

    static func instantiate() -> UIViewController? {
        let storyboard = UIStoryboard(name: "ProductDetail", bundle: .main)
        let viewController = storyboard.instantiateInitialViewController()
        return viewController
    }

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.prepareDetails()
        guard let title = interactor?.product?.title else {
            navigationItem.title = "Detalle del producto"
            return
        }
        navigationItem.title = "\(title)"
    }
}

// MARK: - Private methods

private extension ProductDetailViewController {
    // MARK: Setup

    func setup() {
        ProductDetailConfigurator.configure(self)
    }
}

// MARK: - ProductDetailDisplayLogic

extension ProductDetailViewController: ProductDetailDisplayLogic {

    func displayDetails(_ details: ProductRepresentable) {
        titleLabel.text = details.title
        if let price = details.price {
            priceLabel.isHidden = false
            priceLabel.text = "\(price)"
        } else {
            priceLabel.isHidden = true
        }
        guard let urlString = details.thumbnail, let url = URL(string: urlString) else {
            return
        }
        productImage.sd_setImage(with: url,
                                 placeholderImage: UIImage.init(named: "launchScreen"),
                                 options: .continueInBackground) { [weak self] (image, _, _, _) in
            guard let image = image else {
                return
            }
            self?.productImage.image = image
        }
    }
}
