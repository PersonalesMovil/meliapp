//
//  ProductDetailConfigurator.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct ProductDetailConfigurator {

    static func configure(_ viewController: ProductDetailViewController) {
        let interactor = ProductDetailInteractor()
        let presenter = ProductDetailPresenter()
        viewController.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = viewController
    }

}
