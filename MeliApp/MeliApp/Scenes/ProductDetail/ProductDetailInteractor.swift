//
//  ProductDetailInteractor.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

protocol ProductDetailBusinessLogic {
    func prepareDetails()
}

protocol productDataStore: class {
    var product: ProductModel? { get set }
}

final class ProductDetailInteractor: productDataStore {

    // MARK: - productDataStore

    var product: ProductModel?
    var presenter: ProductDetailPresentationLogic?
}

// MARK: - ProductDetailBusinessLogic

extension ProductDetailInteractor: ProductDetailBusinessLogic {

    func prepareDetails() {
        guard let product = product else {
             return
        }
        let response = ProductDetailResponse(product: product)
        presenter?.presentProduct(response: response)
    }
}
