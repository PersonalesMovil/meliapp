//
//  ProductDetailPresenter.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

protocol ProductDetailPresentationLogic {

    func presentProduct(response: ProductDetailResponse)
}

final class ProductDetailPresenter: ProductDetailPresentationLogic {

    weak var viewController: ProductDetailDisplayLogic?

    // MARK: - ProductDetailPresentationLogic

    func presentProduct(response: ProductDetailResponse) {
        let viewModel = ProductDetailViewModel(productDetailResponse: response)
        viewController?.displayDetails(viewModel)
    }
}
