//
//  SitesConfigurator.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct SitesConfigurator {

    static func configure(_ viewController: SitesViewController) {
        let service = SiteService()
        let interactor = SitesInteractor(service: service)
        let presenter = SitesPresenter()
        let router = SitesRouter()
        viewController.interactor = interactor
        interactor.presenter = presenter
        viewController.router = router
        router.viewController = viewController
        presenter.viewController = viewController
    }

}
