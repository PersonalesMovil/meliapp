//
//  SitesPresenter.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit

protocol SitesPresentationLogic {
    func presentSites(_ response: PrepareSitesResponse)
    func presentMessageError(error: ErrorModel)
}

final class SitesPresenter {
    weak var viewController: SitesDisplayLogic?
}

// MARK: - SitesPresentationLogic

extension SitesPresenter: SitesPresentationLogic {

    func presentMessageError(error: ErrorModel) {
        viewController?.displayError(error)
    }

    func presentSites(_ response: PrepareSitesResponse) {

        let viewModels: [SiteItemViewModel] = response.sites.map {
            SiteItemViewModel(model: $0)
        }
        let sortedViewModel: [SiteItemViewModel] = viewModels.sorted {
            return $0.site.name.compare($1.site.name) == ComparisonResult.orderedAscending
        }
        viewController?.displaySites(sortedViewModel)
    }
}
