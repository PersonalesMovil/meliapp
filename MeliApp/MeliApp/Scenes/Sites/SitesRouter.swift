//
//  SitesRouter.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit

protocol SitesRoutingLogic {
    func navigateToHome(site: SiteModel)
}

final class SitesRouter: NSObject, SitesRoutingLogic {

    weak var viewController: SitesViewController?

    func navigateToHome(site: SiteModel) {
        guard let navigationController = viewController?.navigationController,
              let controller = HomeViewController.instantiate() else {
            return
        }
        KeychainManager.shared.siteID = site.id
        navigationController.pushViewController(controller, animated: true)
    }
}
