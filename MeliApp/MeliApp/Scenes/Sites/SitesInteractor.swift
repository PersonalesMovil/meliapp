//
//  SitesInteractor.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit
import Reachability

protocol SitesBusinessLogic {
    func prepareSites()
}

protocol SitesDataStore {
    var sites: [SiteModel] { get }
}

final class SitesInteractor: SitesDataStore {

    var presenter: SitesPresentationLogic?

    private let service: SiteService

    // MARK: - SitesDataStore

    var sites: [SiteModel] = []

    init(service: SiteService) {
        self.service = service
    }
}

// MARK: - SitesBusinessLogic

extension SitesInteractor: SitesBusinessLogic {

    func prepareSites() {
        guard ReachabilityManager().isConnected == true else {
            presenter?.presentMessageError(error: ErrorModel(code: .notConnection))
            return
        }
        service.fetchSites { [weak self] (sitesModel, error) in
            guard let error = error else {
                guard let sitesModel = sitesModel else {
                    return
                }
                self?.presenter?.presentSites(PrepareSitesResponse(sites: sitesModel))
                return
            }
            self?.presenter?.presentMessageError(error: error)
        }
    }
}
