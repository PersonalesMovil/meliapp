//
//  SitesViewController.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit

protocol SitesDisplayLogic: class {
    func displaySites(_ viewmodel: [SiteItemViewModel])
    func displayError(_ error: ErrorModel)
}

final class SitesViewController: UIViewController {

    var interactor: (SitesBusinessLogic & SitesDataStore)?
    var router: SitesRoutingLogic?

    private var dataProvider = SiteDataProvider(rows: [])

    // MARK: - IBOutlets

    @IBOutlet private weak var tableView: UITableView!

    private lazy var refreshControl: UIRefreshControl = { [unowned self] in
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(refreshSites(sender:)), for: .valueChanged)
        return refreshControl
    }()

    static func instantiate() -> UIViewController? {
        let storyboard = UIStoryboard(name: "Sites", bundle: .main)
        let viewController = storyboard.instantiateInitialViewController()
        return viewController
    }

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        navigationItem.title = "Selecciona tu pais"
        interactor?.prepareSites()
    }
}

extension SitesViewController: SitesDisplayLogic {

    func displayError(_ error: ErrorModel) {
        let alertController = UIAlertController(title: "Error",
                                                message: error.description,
                                                preferredStyle: .alert)
        let action = UIAlertAction(title: "Reintentar",
                                   style: .default) { [weak self] (_) in
            self?.interactor?.prepareSites()
        }
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }

    func displaySites(_ viewmodel: [SiteItemViewModel]) {
        dataProvider.update(rows: viewmodel)
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

// MARK: - Private methods

private extension SitesViewController {

    // MARK: Setup

    func setup() {
        SitesConfigurator.configure(self)
    }

    func setupTableView() {
        tableView.registerCells([SitesViewCell.self])
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        tableView.refreshControl = refreshControl
    }

    // MARK: - Actions

    @objc
    func refreshSites(sender _: UIRefreshControl) {
        interactor?.prepareSites()
        refreshControl.endRefreshing()
    }
}

// MARK: - UITableViewDataSource

extension SitesViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        dataProvider.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataProvider.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = dataProvider[indexPath] else {
            fatalError("View Model was not found")
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SitesViewCell.reuseIdentifier) else {
            fatalError("Cell was not found")
        }
        (cell as? SitesConfigurable)?.configure(with: viewModel)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension SitesViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let viewModel = dataProvider[indexPath] else {
            fatalError("View Model was not found")
        }
        router?.navigateToHome(site: viewModel.site)
    }
}
