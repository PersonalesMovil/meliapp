//
//  ProductsListRouter.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

protocol ProductsRoutingLogic {
    func navigateToProductDetail(product: ProductModel)
}

final class ProductsRouter: NSObject, ProductsRoutingLogic {

    weak var viewController: ProductsViewController?

    func navigateToProductDetail(product: ProductModel) {
        guard let navigationController = viewController?.navigationController,
              let controller = ProductDetailViewController.instantiate() as? ProductDetailViewController else {
            return
        }
        controller.interactor?.product = product
        navigationController.pushViewController(controller, animated: true)
    }
}
