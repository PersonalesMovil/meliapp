//
//  ProductsListConfigurator.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct ProductsConfigurator {

    static func configure(_ viewController: ProductsViewController) {
        let service = CategoryService()
        let interactor = ProductsInteractor(service: service)
        let presenter = ProductsPresenter()
        let router = ProductsRouter()
        viewController.interactor = interactor
        interactor.presenter = presenter
        viewController.router = router
        router.viewController = viewController
        presenter.viewController = viewController
    }

}
