//
//  ProductsListInteractor.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit

protocol ProductsBusinessLogic {
    func prepareProducts()
}

protocol ProductsDataStore {
    var category: CategoryModel? { get set }
    var products: [ProductModel] { get }
}

final class ProductsInteractor: ProductsDataStore {

    var presenter: ProductsPresentationLogic?

    private let service: CategoryService

    // MARK: - ProductsDataStore

    var category: CategoryModel?
    var products: [ProductModel] = []

    init(service: CategoryService) {
        self.service = service
    }
}

// MARK: - ProductsBusinessLogic

extension ProductsInteractor: ProductsBusinessLogic {

    func prepareProducts() {
        guard let categoryId = category?.id else {
            return
        }
        service.fetchProducts(byCategoryId: categoryId) { [weak self] (productsResponse) in
            guard let productResponse = productsResponse else {
                return
            }
            self?.presenter?.presentProducts(productResponse)
        }
    }
}
