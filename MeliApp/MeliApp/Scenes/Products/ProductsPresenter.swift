//
//  ProductsListPresenter.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit

protocol ProductsPresentationLogic {
    func presentProducts(_ response: ProductsResponse)
}

final class ProductsPresenter {

    weak var viewController: ProductsDisplayLogic?
}

// MARK: - ProductsPresentationLogic

extension ProductsPresenter: ProductsPresentationLogic {

    func presentProducts(_ response: ProductsResponse) {
        guard let productsModel = response.results else {
            return
        }
        let viewModels: [ProductItemViewModel] = productsModel.map {
            ProductItemViewModel(model: $0)
        }
        viewController?.displayProducts(viewModels)
    }
}
