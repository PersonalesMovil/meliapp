//
//  ProductsListViewController.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit

protocol ProductsDisplayLogic: class {
    func displayProducts(_ viewmodel: [ProductItemViewModel])
}

final class ProductsViewController: UIViewController {

    var interactor: (ProductsBusinessLogic & ProductsDataStore)?
    var router: ProductsRoutingLogic?

    private var dataProvider = ProductDataProvider(rows: [])

    // MARK: - IBOutlets

    @IBOutlet private weak var tableView: UITableView!

    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .black
        refreshControl.addTarget(self, action: #selector(refreshProducts(sender:)), for: .valueChanged)
        return refreshControl
    }()

    static func instantiate() -> UIViewController? {
        let storyboard = UIStoryboard(name: "Products", bundle: .main)
        let viewController = storyboard.instantiateInitialViewController()
        return viewController
    }

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        interactor?.prepareProducts()
        guard let title = interactor?.category?.name else {
            navigationItem.title = "Productos"
            return
        }
        navigationItem.title = "\(title)"
    }
}

extension ProductsViewController: ProductsDisplayLogic {
    func displayProducts(_ viewmodel: [ProductItemViewModel]) {
        dataProvider.update(rows: viewmodel)
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

// MARK: - Private methods
private extension ProductsViewController {

    // MARK: Setup

    func setup() {
        ProductsConfigurator.configure(self)
    }

    func setupTableView() {
        tableView.registerCells([ProductTableViewCell.self])
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .clear
        tableView.refreshControl = refreshControl
    }

    // MARK: - Actions

    @objc
    func refreshProducts(sender _: UIRefreshControl) {
        interactor?.prepareProducts()
        refreshControl.endRefreshing()
    }

}

extension ProductsViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        dataProvider.numberOfSections()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataProvider.numberOfItems(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = dataProvider[indexPath] else {
            fatalError("View Model was not found")
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.reuseIdentifier) else {
            fatalError("Cell was not found")
        }
        (cell as? ProductConfigurable)?.configure(with: viewModel)
        return cell
    }
}

extension ProductsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let viewModel = dataProvider[indexPath] else {
            fatalError("View Model was not found")
        }
        router?.navigateToProductDetail(product: viewModel.product)
    }
}
