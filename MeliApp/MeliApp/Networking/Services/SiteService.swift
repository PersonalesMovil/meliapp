//
//  SiteService.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 21/11/20.
//

import Foundation

final class SiteService {

    func fetchSites(completionHandler: @escaping ([SiteModel]?, ErrorModel?) -> Void) {
        let task = URLSession.shared.dataTask(with: APIRouter.getSites.asURL()) { (data, response, _) in
            guard let data = data else {
                guard let httpURLResponse = response as? HTTPURLResponse else {
                    return
                }
                switch httpURLResponse.statusCode {
                case 400:
                    completionHandler(nil, ErrorModel(code: .badRequest))
                case 404:
                    completionHandler(nil, ErrorModel(code: .notFound))
                case 500:
                    completionHandler(nil, ErrorModel(code: .errorServer))
                default:
                    completionHandler(nil, ErrorModel(code: .unknown))
                }
                return
            }
            do {
                let sites = try JSONDecoder().decode([SiteModel].self, from: data)
                completionHandler(sites, nil)
            } catch let error {
                completionHandler(nil, ErrorModel(code: .other, descriptionLocalizable: error.localizedDescription))
            }
        }
        task.resume()
    }
}
