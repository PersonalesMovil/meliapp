//
//  CategoryService.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

final class CategoryService {

    func fetchCategories(completionHandler: @escaping ([CategoryModel]?) -> Void) {
        let task = URLSession.shared.dataTask(with: APIRouter.getCategories.asURL()) { (data, _, _) in
            guard let data = data else {
                completionHandler(nil)
                return
            }
            do {
                let categories = try JSONDecoder().decode([CategoryModel].self, from: data)
                completionHandler(categories)
            } catch {
                completionHandler(nil)
            }
        }
        task.resume()
    }

    func fetchProducts(byCategoryId: String, completionHandler: @escaping (ProductsResponse?) -> Void) {
        let task = URLSession.shared.dataTask(with: APIRouter.getProducts(byCategoryId: byCategoryId).asURL()) { (data, _, _) in
            guard let data = data else {
                completionHandler(nil)
                return
            }
            do {
                let productsResponse = try JSONDecoder().decode(ProductsResponse.self, from: data)
                completionHandler(productsResponse)
            } catch {
                completionHandler(nil)
            }
        }
        task.resume()
    }

    func fetchProducts(byQuery: String, completionHandler: @escaping (ProductsResponse?) -> Void) {
        let task = URLSession.shared.dataTask(with: APIRouter.searchProducts(byQuery: byQuery).asURL()) { (data, _, _) in
            guard let data = data else {
                completionHandler(nil)
                return
            }
            do {
                let productsResponse = try JSONDecoder().decode(ProductsResponse.self, from: data)
                completionHandler(productsResponse)
            } catch {
                completionHandler(nil)
            }
        }
        task.resume()
    }
}
