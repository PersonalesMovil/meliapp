//
//  APIManager.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

enum APIRouter {

    case getSites
    case getCategories
    case getProducts(byCategoryId: String)
    case searchProducts(byQuery: String)

    var path: String {
        let siteId = KeychainManager.shared.siteID ?? ""
        switch self {
        case .getSites:
            return "/"
        case .getCategories:
            return "/\(siteId)/categories"
        case .getProducts(let byCategoryId):
            return "/\(siteId)/search?category=\(byCategoryId)"
        case .searchProducts(let byQuery):
            return "/\(siteId)/search?q==\(byQuery)"
        }
    }

    func asURL() -> URL {
        let urlString = Config.apiBaseUrl.appending(path).replacingOccurrences(of: " ", with: "%20")
        // swiftlint:disable:next force_unwrapping
        return URL(string: urlString)!
    }
}

struct Config {
    static let apiBaseUrl = "https://api.mercadolibre.com/sites"
}
