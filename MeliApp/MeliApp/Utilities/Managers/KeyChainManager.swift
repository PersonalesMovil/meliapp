//
//  KeyChainManager.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import KeychainAccess

final class KeychainManager {
    private let keychain: Keychain

    static let shared = KeychainManager()

    // MARK: - Configuration + Keys

    private enum KeychainConfiguration {
        static let serviceName = "com.mercadoLibre.test"
        static let SiteID = "siteID"
    }

    init() {
        keychain = Keychain(service: KeychainConfiguration.serviceName)
    }
    /**
     user Site ID Selected
     */
    var siteID: String? {
        get {
            return try? keychain.getString(KeychainConfiguration.SiteID)
        }
        set {
            if let newValue = newValue {
                store(value: String(newValue), with: KeychainConfiguration.SiteID)
            } else {
                store(value: nil, with: KeychainConfiguration.SiteID)
            }
        }
    }
}

// MARK: - Helpers functions

private extension KeychainManager {
    func store(value: String?, with key: String) {
        if let value = value {
            try? keychain.set(value, key: key)
        } else {
            try? keychain.remove(key)
        }
    }
}
