//
//  ReachabilityManager.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 23/11/20.
//

import Foundation
import Reachability

struct ReachabilityManager {

    ///returns the current state of the connection
    var isConnected: Bool {
        do {
            let reachability = try Reachability()
            return reachability.connection != .unavailable
        } catch {
            print("Unable to start notifier")
        }
        return false
    }
}
