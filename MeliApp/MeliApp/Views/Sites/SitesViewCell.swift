//
//  SitesViewCell.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit

protocol SitesConfigurable {
    func configure(with model: SiteItemViewModel)
}

final class SitesViewCell: UITableViewCell {

    // MARK: - IBOutlets

    @IBOutlet private weak var siteNameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: - SitesConfigurable

extension SitesViewCell: SitesConfigurable {

    func configure(with model: SiteItemViewModel) {
        siteNameLabel.text = model.site.name
    }
}
