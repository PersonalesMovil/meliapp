//
//  CategoryTableViewCell.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit

final class CategoryTableViewCell: UITableViewCell {

    // MARK: - IBOutlets

    @IBOutlet private weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: - HomeSectionConfigurable

extension CategoryTableViewCell: HomeSectionConfigurable {
    func configure(viewModel: HomeSectionRepresentable?) {
        guard let model = viewModel?.model as? HomeCategorySection else {
            return
        }
        nameLabel.text = model.item.name
    }
}
