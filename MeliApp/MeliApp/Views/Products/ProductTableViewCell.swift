//
//  ProductTableViewCell.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit
import SDWebImage

final class ProductTableViewCell: UITableViewCell {

    // MARK: - IBOutlets

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var availableQuantityLabel: UILabel!
    @IBOutlet private weak var productImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

private extension ProductTableViewCell {
    func setup(model: ProductModel) {
        nameLabel.text = model.title
        if let price = model.price {
            priceLabel.isHidden = false
            priceLabel.text = "Price: \(price)"
        } else {
            priceLabel.isHidden = true
        }
        if let availableQuantity = model.availableQuantity {
            availableQuantityLabel.text = "Quantity: \(availableQuantity)"
        }
        guard let urlString = model.thumbnail, let url = URL(string: urlString) else {
            return
        }
        productImage.sd_setImage(with: url,
                                 placeholderImage: UIImage.init(named: "launchScreen"),
                                 options: .continueInBackground) { [weak self] (image, _, _, _) in
            guard let image = image else {
                return
            }
            self?.productImage.image = image
        }
    }
}

// MARK: - HomeSectionConfigurable

extension ProductTableViewCell: HomeSectionConfigurable {
    func configure(viewModel: HomeSectionRepresentable?) {
        guard let model = viewModel?.model as? HomeSearchSection else {
            return
        }
        setup(model: model.item)
    }
}

// MARK: - ProductConfigurable

extension ProductTableViewCell: ProductConfigurable {
    func configure(with viewModel: ProductItemViewModel) {
        setup(model: viewModel.product)
    }
}
