//
//  CategoryDataProvider.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

final class CategoryDataProvider: ArrayDataProvider<CategoryItemViewModel> {}
