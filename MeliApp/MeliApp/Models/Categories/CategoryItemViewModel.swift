//
//  CategoryItemViewModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

final class CategoryItemViewModel {

    var category: CategoryModel

    init(model: CategoryModel) {
        category = model
    }
}

extension CategoryItemViewModel: Equatable {
    static func == (lhs: CategoryItemViewModel, rhs: CategoryItemViewModel) -> Bool {
        return lhs.category.name == lhs.category.name
    }
}
