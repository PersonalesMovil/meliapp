//
//  PrepareCategoriesResponse.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct PrepareCategoriesResponse {
    var categories: [CategoryModel]
}
