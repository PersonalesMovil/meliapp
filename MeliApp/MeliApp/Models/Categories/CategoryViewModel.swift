//
//  CategoryViewModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct CategoryViewModel: CategoryRepresentable {

    let category: CategoryModel

    init(_ model: CategoryModel) {
        self.category = model
        name = model.name
    }

    // MARK: - CategoryRepresentable
    var name: String?
}

extension CategoryViewModel: Equatable {

    static func == (lhs: CategoryViewModel, rhs: CategoryViewModel) -> Bool {
        return lhs.category.name == rhs.category.name
    }
}

protocol CategoryConfigurable {
    func configure(with viewModel: CategoryRepresentable)
}

protocol CategoryRepresentable {
    var name: String? { get }
}
