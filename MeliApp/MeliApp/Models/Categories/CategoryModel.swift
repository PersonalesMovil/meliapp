//
//  CategoryModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct CategoryModel {

    let id: String
    let name: String?
    let permalink: String?
    let totalItemsInThisCategory: String?
    let pathFromRoot: [PathFromRoot]?
    let childrenCategories: [ChildrenCategories]?

    struct PathFromRoot: Codable {
        let id: String
        let name: String?
    }

    struct ChildrenCategories: Codable {
        let id: String
        let name: String?
        let totalItemsInThisCategory: String?
    }
}

extension CategoryModel: Codable {

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case permalink
        case totalItemsInThisCategory = "total_items_in_this_category"
        case pathFromRoot = "path_from_root"
        case childrenCategories = "children_categories"
    }
}
