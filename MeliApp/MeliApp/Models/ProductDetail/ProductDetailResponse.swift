//
//  ProductDetailResponse.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 23/11/20.
//

import Foundation

struct ProductDetailResponse {
    let product: ProductModel
}
