//
//  ProductDetailViewModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 23/11/20.
//

import Foundation

struct ProductDetailViewModel: ProductRepresentable {

    var id: String
    var title: String?
    var siteId: String?
    var price: Double?
    var currencyId: String?
    var availableQuantity: Int?
    var thumbnail: String?

    init(productDetailResponse: ProductDetailResponse) {
        self.id = productDetailResponse.product.id
        self.title = productDetailResponse.product.title
        self.siteId = productDetailResponse.product.siteId
        self.price = productDetailResponse.product.price
        self.availableQuantity = productDetailResponse.product.availableQuantity
        self.thumbnail = productDetailResponse.product.thumbnail
    }
}
