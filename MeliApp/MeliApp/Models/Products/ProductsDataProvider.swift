//
//  ProductsDataProvider.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

final class ProductDataProvider: ArrayDataProvider<ProductItemViewModel> {}
