//
//  ProductModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct ProductModel {

    let id: String
    let title: String?
    let siteId: String?
    let price: Double?
    let currencyId: String?
    let availableQuantity: Int?
    let thumbnail: String?
}

extension ProductModel: Codable {

    enum CodingKeys: String, CodingKey {
        case id
        case siteId = "site_id"
        case title
        case price
        case currencyId = "currency_id"
        case availableQuantity = "available_quantity"
        case thumbnail
    }
}
