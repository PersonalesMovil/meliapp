//
//  ProductItemViewModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

final class ProductItemViewModel {

    var product: ProductModel

    init(model: ProductModel) {
        product = model
    }
}

extension ProductItemViewModel: Equatable {
    static func == (lhs: ProductItemViewModel, rhs: ProductItemViewModel) -> Bool {
        return lhs.product.title == lhs.product.title
    }
}
