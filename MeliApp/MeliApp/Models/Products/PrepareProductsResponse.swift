//
//  PrepareProductsResponse.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct ProductsResponse {

    struct Paging: Codable {
        var total: Int?
        var offset: Int?
        var limit: Int?
    }

    var siteId: String?
    var paging: Paging?
    var result: [ProductModel]?
}

extension ProductsResponse: Codable {

    enum CodingKeys: String, CodingKey {
        case siteId = "site_id"
        case paging
        case result
    }
}
