//
//  ProductViewModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct ProductViewModel {

    let product: ProductModel

    init(_ model: ProductModel) {
        self.product = model
    }
}

extension ProductViewModel: Equatable {

    static func == (lhs: ProductViewModel, rhs: ProductViewModel) -> Bool {
        return lhs.product.title == rhs.product.title
    }
}

protocol ProductConfigurable {
    func configure(with viewModel: ProductItemViewModel)
}

protocol ProductRepresentable {

    var id: String { get }
    var title: String? { get }
    var siteId: String? { get }
    var price: Double? { get }
    var currencyId: String? { get }
    var availableQuantity: Int? { get }
    var thumbnail: String? { get }
}
