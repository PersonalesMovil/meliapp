//
//  ErrorModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 23/11/20.
//

import Foundation

struct ErrorModel: ErrorRepresentable {

    var description: String? {
        switch code {
        case .notConnection:
            return "Por favor revise su conexion a internet e intente nuevamente"
        case .unknown, .badRequest, .notFound:
            return "Algo salio mal, revise su conexión e intente nuevamente"
        case .errorServer:
            return "Algo salio mal, el servicio no se encuentra disponible, por favor intente mas tarde"
        case .other:
            return descriptionLocalizable
        }
    }
    var code: ErrorCode
    var descriptionLocalizable: String?
}

protocol ErrorRepresentable {
    var code: ErrorCode { get set }
}

enum ErrorCode {
    case notConnection
    case unknown
    case badRequest
    case errorServer
    case notFound
    case other
}
