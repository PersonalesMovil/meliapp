//
//  HomeSectionsResponse.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct HomeSectionsResponse {

    let sections: [HomeSection]
}
