//
//  HomeViewModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import UIKit

final class HomeSectionViewModel: HomeSection, HomeSectionRepresentable {

    var rowHeight: CGFloat = 44 // Default

    var model: HomeSection

    // MARK: - TableViewCompatible

    var reuseIdentifier: String

    init(with model: HomeSection) {
        self.model = model
        self.reuseIdentifier = HomeCellReuseIdentifierFactory.reuseIdentifier(forSectionType: self.model.type)
        super.init(for: self.model.type)
    }
}

// Helpers

protocol HomeSectionRepresentable {

    var rowHeight: CGFloat { get set }
    var model: HomeSection { get set }

}

enum HomeCellReuseIdentifierFactory {

    static func reuseIdentifier(forSectionType type: HomeSectionType) -> String {
        switch type {
        case .categories:
            return CategoryTableViewCell.reuseIdentifier
        case .search:
            return ProductTableViewCell.reuseIdentifier
        }
    }
}

protocol HomeSectionConfigurable {

    func configure(viewModel: HomeSectionRepresentable?)

}
