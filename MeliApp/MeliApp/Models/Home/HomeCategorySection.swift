//
//  HomeCategorySection.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

final class HomeCategorySection: HomeSection {

    var item: CategoryModel
    var name: String?

    init(for type: HomeSectionType, category: CategoryModel) {
        self.item = category
        super.init(for: type)
    }
}
