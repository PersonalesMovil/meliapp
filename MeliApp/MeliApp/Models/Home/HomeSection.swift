//
//  HomeSection.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

class HomeSection {

    private(set) var type: HomeSectionType
    private(set) var position: Int

    init(for type: HomeSectionType) {
        self.type = type
        self.position = type.rawValue
    }
}

extension HomeSection: Equatable {

    static func == (lhs: HomeSection, rhs: HomeSection) -> Bool {
        return lhs.type == rhs.type && lhs.position == rhs.position
    }
}
