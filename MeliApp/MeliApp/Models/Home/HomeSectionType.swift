//
//  HomeSectionType.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

enum HomeSectionType: Int {
    case categories
    case search
}
