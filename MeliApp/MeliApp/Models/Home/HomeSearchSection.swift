//
//  HomeSearchSection.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

final class HomeSearchSection: HomeSection {

    var item: ProductModel

    init(for type: HomeSectionType, product: ProductModel) {
        self.item = product
        super.init(for: type)
    }
}
