//
//  SiteModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 21/11/20.
//

struct SiteModel {

    let defaultCurrencyId: String
    let id: String
    let name: String
}

extension SiteModel: Codable {

    enum CodingKeys: String, CodingKey {

        case defaultCurrencyId = "default_currency_id"
        case id
        case name

    }
}
