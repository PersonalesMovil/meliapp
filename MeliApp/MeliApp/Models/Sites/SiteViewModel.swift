//
//  SiteViewModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

struct SiteViewModel: SiteRepresentable {

    let site: SiteModel

    init(_ model: SiteModel) {
        self.site = model
        name = model.name
    }

    // MARK: - SiteRepresentable
    var name: String
}

extension SiteViewModel: Equatable {

    static func == (lhs: SiteViewModel, rhs: SiteViewModel) -> Bool {
        return lhs.site.name == rhs.site.name
    }
}

protocol SiteConfigurable {
    func configure(with viewModel: SiteRepresentable)
}

protocol SiteRepresentable {
    var name: String { get }
}
