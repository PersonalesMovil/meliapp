//
//  SiteItemViewModel.swift
//  MeliApp
//
//  Created by Sunbelt Factory on 22/11/20.
//

import Foundation

class SiteItemViewModel {

    var site: SiteModel

    init(model: SiteModel) {
        site = model
    }
}

extension SiteItemViewModel: Equatable {
    static func == (lhs: SiteItemViewModel, rhs: SiteItemViewModel) -> Bool {
        return lhs.site.name == lhs.site.name
    }
}
