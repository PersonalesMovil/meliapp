# Mercado Libre App #

Allows to consume public api from Mercado Libre

### What is this repository for? ###

* Native iOS App
* Version Swift 5

### How do I get set up? ###

* Install Cocoapods v1.10
* Install swiftlint v0.40
* execute pod Install
* Viper design pattern

### Contribution guidelines ###

* perform pull to request if you want to add some functionality

### Who do I talk to? ###

* Jose Daniel Avalos
* Team Mobile